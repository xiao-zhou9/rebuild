/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.support.integration;

import cn.devezhao.commons.CalendarUtils;
import cn.devezhao.commons.CodecUtils;
import cn.hutool.core.io.FileUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.qiniu.common.QiniuException;
import com.qiniu.http.Client;
import com.qiniu.http.Response;
import com.qiniu.storage.BucketManager;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.Region;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.FileInfo;
import com.qiniu.util.Auth;
import com.qiniu.util.StringMap;
import com.rebuild.core.Application;
import com.rebuild.core.RebuildException;
import com.rebuild.core.cache.CommonsCache;
import com.rebuild.core.support.ConfigurationItem;
import com.rebuild.core.support.RebuildConfiguration;
import com.rebuild.utils.CommonsUtils;
import com.rebuild.utils.OkHttpUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.util.Assert;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Calendar;


@Slf4j
public class QiniuCloud {

    
    public static final Configuration CONFIGURATION = new Configuration(Region.autoRegion());

    private static final QiniuCloud INSTANCE = new QiniuCloud();

    public static QiniuCloud instance() {
        return INSTANCE;
    }

    

    private final UploadManager UPLOAD_MANAGER = new UploadManager(CONFIGURATION);

    private Auth auth;
    private String bucketName;

    private QiniuCloud() {
        initAuth();
    }

    
    public boolean available() {
        return this.auth != null;
    }

    
    public void initAuth() {
        String[] account = RebuildConfiguration.getStorageAccount();
        if (account != null) {
            this.auth = Auth.create(account[0], account[1]);
            this.bucketName = account[2];
        } else {
            log.info("No QiniuCloud configuration! Using local storage.");
        }
    }

    
    public Auth getAuth() {
        Assert.notNull(auth, "Qiniu account not config");
        return auth;
    }

    
    public String upload(File file, String fops) throws IOException {
        String fileKey = formatFileKey(file.getName());
        Response resp = UPLOAD_MANAGER.put(file, fileKey, getUploadToken(fileKey, fops));
        if (resp.isOK()) {
            return fileKey;
        } else {
            log.error("Cannot upload file : {}. Resp: {}", file.getName(), resp);
            return null;
        }
    }

    
    public String upload(File file) throws IOException {
        return upload(file, null);
    }

    
    public String upload(URL url) throws Exception {
        File tmp = OkHttpUtils.readBinary(url.toString());
        if (tmp == null) {
            throw new RebuildException("Cannot read file from URL : " + url);
        }

        try {
            return upload(tmp);
        } finally {
            FileUtils.deleteQuietly(tmp);
        }
    }

    
    public String makeUrl(String filePath) {
        return makeUrl(filePath, 3 * 60);
    }

    
    public String makeUrl(String filePath, int seconds) {
        String baseUrl = RebuildConfiguration.getStorageUrl() + filePath;
        
        if (baseUrl.startsWith("//")) {
            baseUrl = "https:" + baseUrl;
        }

        long deadline = System.currentTimeMillis() / 1000 + seconds;
        if (seconds > 60) {
            Calendar c = CalendarUtils.getInstance();
            c.add(Calendar.SECOND, seconds);
            c.set(Calendar.SECOND, 59);  
            deadline = c.getTimeInMillis() / 1000;
        }

        
        return getAuth().privateDownloadUrlWithDeadline(baseUrl, deadline);
    }

    
    public FileInfo stat(String filePath) {
        BucketManager bucketManager = new BucketManager(getAuth(), CONFIGURATION);
        try {
            return bucketManager.stat(this.bucketName, filePath);
        } catch (QiniuException e) {
            log.error("Cannot stat file : {}", filePath);
        }
        return null;
    }

    
    public boolean delete(String key) {
        BucketManager bucketManager = new BucketManager(getAuth(), CONFIGURATION);
        Response resp;
        try {
            resp = bucketManager.delete(this.bucketName, key);
            if (resp.isOK()) {
                return true;
            } else {
                throw new RebuildException("Failed to delete file : " + this.bucketName + " < " + key + " : " + resp.bodyString());
            }
        } catch (QiniuException e) {
            throw new RebuildException("Failed to delete file : " + this.bucketName + " < " + key, e);
        }
    }

    
    @SuppressWarnings("deprecation")
    public long stats() {
        String time = CalendarUtils.getPlainDateFormat().format(CalendarUtils.now());
        String url = String.format(
                "%s/v6/space?bucket=%s&begin=%s000000&end=%s235959&g=day",
                CONFIGURATION.apiHost(), bucketName, time, time);
        StringMap headers = getAuth().authorization(url);

        try {
            Client client = new Client(CONFIGURATION);
            Response resp = client.get(url, headers);
            if (resp.isOK()) {
                JSONObject map = JSON.parseObject(resp.bodyString());
                return map.getJSONArray("datas").getLong(0);
            }

        } catch (QiniuException e) {
            log.warn(null, e);
        }
        return -1;
    }

    
    public String getUploadToken(String fileKey, String fops) {
        
        int maxSize = RebuildConfiguration.getInt(ConfigurationItem.PortalUploadMaxSize);
        StringMap policy = new StringMap().put("fsizeLimit", FileUtils.ONE_MB * maxSize);
        if (fops != null) policy.put("persistentOps", fops).put("persistentNotifyUrl", "https://webhook.site/e2784dd3-cf2c-49ce-8d53-05666e7f5bd0");

        return getAuth().uploadToken(bucketName, fileKey, 3600L, policy, true);
    }

    
    public String getUploadToken(String fileKey) {
        return getUploadToken(fileKey, null);
    }

    
    public void download(String filePath, File dest) throws IOException {
        String url = makeUrl(filePath);
        OkHttpUtils.readBinary(url, dest, null);
    }

    

    
    public static String formatFileKey(String fileName) {
        return formatFileKey(fileName, true);
    }

    
    public static String formatFileKey(String fileName, boolean keepName) {
        if (keepName) {
            while (fileName.contains("__")) {
                fileName = fileName.replace("__", "_");
            }
            
            fileName = fileName.replaceAll("[?&#+%/\\s]", "");

            
            if (fileName.length() > 51) {
                fileName = fileName.substring(0, 25) + ".." + fileName.substring(fileName.length() - 25);
            }

        } else {
            String fileExt = FileUtil.getSuffix(fileName);
            fileName = CommonsUtils.randomHex(true).substring(0, 20);
            if (StringUtils.isNotBlank(fileExt)) fileName += "." + fileExt;
        }

        String datetime = CalendarUtils.getDateFormat("yyyyMMddHHmmssSSS").format(CalendarUtils.now());
        fileName = String.format("rb/%s/%s__%s", datetime.substring(0, 8), datetime.substring(8), fileName);
        return fileName;
    }

    
    public static String parseFileName(String filePath) {
        String[] filePathSplit = filePath.split("/");
        String fileName = filePathSplit[filePathSplit.length - 1];
        if (fileName.contains("__")) {
            fileName = fileName.substring(fileName.indexOf("__") + 2);
        }
        return fileName;
    }

    
    public static String encodeUrl(String url) {
        if (StringUtils.isBlank(url)) {
            return url;
        }

        String[] urlSplit = url.split("/");
        for (int i = 0; i < urlSplit.length; i++) {
            String e = CodecUtils.urlEncode(urlSplit[i]);
            if (e.contains("+")) {
                e = e.replace("+", "%20");
            }
            urlSplit[i] = e;
        }
        return StringUtils.join(urlSplit, "/");
    }

    
    public static long getStorageSize() {
        Long size = (Long) Application.getCommonsCache().getx("_StorageSize");
        if (size != null) return size;

        if (QiniuCloud.instance().available()) {
            size = QiniuCloud.instance().stats();
        } else {
            File data = RebuildConfiguration.getFileOfData("rb");
            if (data.exists()) {
                size = FileUtils.sizeOfDirectory(data);
            }
        }

        if (size == null) size = 0L;

        Application.getCommonsCache().putx("_StorageSize", size, CommonsCache.TS_HOUR);
        return size;
    }

    
    public static File getStorageFile(String filePath) throws IOException, RebuildException {
        File file;
        if (filePath.startsWith("http://") || filePath.startsWith("https://")) {
            String name = filePath.split("\\?")[0];
            name = name.substring(name.lastIndexOf("/") + 1);
            file = RebuildConfiguration.getFileOfTemp("down" + System.nanoTime() + "." + name);
            OkHttpUtils.readBinary(filePath, file, null);

        } else if (QiniuCloud.instance().available()) {
            String name = parseFileName(filePath);
            file = RebuildConfiguration.getFileOfTemp("down" + System.nanoTime() + "." + name);
            instance().download(filePath, file);

        } else {
            file = RebuildConfiguration.getFileOfData(filePath);
        }

        if (!file.exists()) throw new RebuildException("Cannot read file : " + filePath);
        return file;
    }
}

/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.support;

import cn.devezhao.persist4j.engine.ID;
import com.rebuild.core.UserContextHolder;


public abstract class SetUser {

    private ID user;

    
    public SetUser setUser(ID user) {
        this.user = user;
        return this;
    }

    
    public ID getUser() {
        return user != null ? user : UserContextHolder.getUser();
    }
}

/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.service.approval;

import cn.devezhao.persist4j.engine.ID;
import com.alibaba.fastjson.JSONObject;
import com.rebuild.core.service.query.QueryHelper;

import java.util.HashSet;
import java.util.Set;


public class FlowBranch extends FlowNode {

    private int priority;

    private Set<String> childNodes = new HashSet<>();
    private String lastNode;

    
    protected FlowBranch(String nodeId, int priority, JSONObject dataMap) {
        super(nodeId, TYPE_BRANCH, dataMap);
        this.priority = priority;
    }

    
    public int getPriority() {
        return priority;
    }

    
    protected void addNode(String child) {
        childNodes.add(child);
        lastNode = child;
    }

    
    protected Set<String> getChildNodes() {
        return childNodes;
    }

    
    protected String getLastNode() {
        return lastNode;
    }

    
    public boolean matches(ID record) {
        return QueryHelper.isMatchAdvFilter(record, (JSONObject) getDataMap().get("filter"));
    }

    @Override
    public String toString() {
        return super.toString() + ", Priority:" + getPriority();
    }

    
    public static FlowBranch valueOf(JSONObject node) {
        return new FlowBranch(
                node.getString("nodeId"), node.getIntValue("priority"), node.getJSONObject("data"));
    }
}

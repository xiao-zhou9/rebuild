/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.service.general;

import cn.devezhao.persist4j.engine.ID;
import com.rebuild.core.Application;
import com.rebuild.core.metadata.EntityHelper;
import com.rebuild.core.metadata.MetadataHelper;
import com.rebuild.core.service.NoRecordFoundException;
import com.rebuild.core.service.query.QueryHelper;
import com.rebuild.core.support.general.FieldValueHelper;
import org.apache.commons.lang.StringUtils;

import java.util.*;


@SuppressWarnings("unchecked")
public class RecentlyUsedHelper {

    
    private static final int MAXNUM_PRE_ENTITY = 50;

    
    public static ID[] gets(ID user, String entity, String type) {
        return gets(user, entity, type, 10, null);
    }

    
    public static ID[] gets(ID user, String entity, String type, String checkFilter) {
        return gets(user, entity, type, 10, checkFilter);
    }

    
    protected static ID[] gets(ID user, String entity, String type, int limit, String checkFilter) {
        final String ckey = formatKey(user, entity, type);
        LinkedList<ID> cached = (LinkedList<ID>) Application.getCommonsCache().getx(ckey);
        if (cached == null || cached.isEmpty()) return ID.EMPTY_ID_ARRAY;

        Set<ID> missed = new HashSet<>();
        List<ID> data = new ArrayList<>();

        for (int i = 0; i < limit && i < cached.size(); i++) {
            final ID raw = cached.get(i);

            boolean allowRead = raw.getEntityCode() == EntityHelper.ClassificationData
                    || Application.getPrivilegesManager().allowRead(user, raw);
            if (!allowRead) continue;

            
            if (checkFilter != null) {
                if (!QueryHelper.isMatchFilter(raw, checkFilter)) {
                    continue;
                }
            }

            try {
                ID clone = ID.valueOf(raw.toLiteral());
                clone.setLabel(FieldValueHelper.getLabel(raw));
                data.add(clone);
            } catch (NoRecordFoundException ex) {
                missed.add(raw);
            }
        }

        if (!missed.isEmpty()) {
            cached.removeAll(missed);
            Application.getCommonsCache().putx(ckey, cached);
        }

        return data.toArray(new ID[0]);
    }

    
    public static void add(ID user, ID id, String type) {
        final String key = formatKey(user, MetadataHelper.getEntityName(id), type);
        LinkedList<ID> cached = (LinkedList<ID>) Application.getCommonsCache().getx(key);
        if (cached == null) {
            cached = new LinkedList<>();
        } else {
            cached.remove(id);
        }

        if (cached.size() > MAXNUM_PRE_ENTITY) {
            cached.removeLast();
        }

        cached.addFirst(id);
        Application.getCommonsCache().putx(key, cached);
    }

    
    public static void clean(ID user, String entity, String type) {
        final String key = formatKey(user, entity, type);
        Application.getCommonsCache().evict(key);
    }

    private static String formatKey(ID user, String entity, String type) {
        return String.format("RS31.%s-%s-%s", user, entity, StringUtils.defaultIfBlank(type, StringUtils.EMPTY));
    }
}

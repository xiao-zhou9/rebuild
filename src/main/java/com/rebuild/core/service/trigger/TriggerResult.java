/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.service.trigger;

import cn.devezhao.persist4j.engine.ID;
import com.alibaba.fastjson.JSONAware;
import com.alibaba.fastjson.JSONObject;
import com.rebuild.utils.JSONUtils;

import java.util.Collection;


public class TriggerResult implements JSONAware {

    
    private int level;
    
    private String message;
    
    private Collection<ID> affected;

    private TriggerSource chain;

    protected TriggerResult(int level, String message, Collection<ID> affected) {
        this.level = level;
        this.message = message;
        this.affected = affected;
    }

    protected void setChain(TriggerSource chain) {
        this.chain = chain;
    }

    public boolean hasAffected() {
        return affected != null && !affected.isEmpty();
    }
    
    @Override
    public String toJSONString() {
        JSONObject res = JSONUtils.toJSONObject("level", level);
        if (message != null) res.put("message", message);
        if (affected != null) res.put("affected", affected);
        if (chain != null) res.put("chain", chain.toString());
        return res.toJSONString();
    }

    @Override
    public String toString() {
        return toJSONString();
    }

    
    public static TriggerResult success(Collection<ID> affected) {
        return new TriggerResult(1, null, affected);
    }

    
    public static TriggerResult success(String message) {
        return new TriggerResult(1, message, null);
    }

    
    public static TriggerResult success(String message, Collection<ID> affected) {
        return new TriggerResult(1, message, affected);
    }

    
    public static TriggerResult wran(String message) {
        return new TriggerResult(2, message, null);
    }

    
    public static TriggerResult error(String message) {
        return new TriggerResult(3, message, null);
    }

    
    public static TriggerResult noMatching() {
        return wran("No matching");
    }

    
    public static TriggerResult triggerOnce() {
        return wran("Trigger once");
    }

    
    public static TriggerResult targetEmpty() {
        return wran("Target are empty");
    }

    
    public static TriggerResult targetSame() {
        return wran("Target are same");
    }

    
    public static TriggerResult targetNotExists() {
        return wran("Target not exists");
    }

    
    public static TriggerResult noUpdateFields() {
        return wran("No update fields");
    }

    
    public static TriggerResult badConfig() {
        return wran("Bad config");
    }
}

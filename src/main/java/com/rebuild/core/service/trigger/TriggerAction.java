/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.service.trigger;

import cn.devezhao.persist4j.Record;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.rebuild.core.service.general.OperatingContext;


public abstract class TriggerAction {

    
    public static final String SOURCE_SELF = "$PRIMARY$";
    
    public static final String TARGET_ANY = "$";

    final protected ActionContext actionContext;

    protected TriggerAction(ActionContext actionContext) {
        this.actionContext = actionContext;
    }

    public ActionContext getActionContext() {
        return actionContext;
    }

    abstract public ActionType getType();

    abstract public Object execute(OperatingContext operatingContext) throws TriggerException;

    
    protected void prepare(OperatingContext operatingContext) throws TriggerException {
    }

    
    public void clean() {
    }

    
    public boolean isUsableSourceEntity(int entityCode) {
        return true;
    }

    
    protected boolean hasUpdateFields(OperatingContext operatingContext) {
        final JSONObject content = (JSONObject) actionContext.getActionContent();
        JSONArray whenUpdateFields = content.getJSONArray("whenUpdateFields");
        if (whenUpdateFields == null || whenUpdateFields.isEmpty()) return true;

        Record updatedRecord = operatingContext.getAfterRecord();
        boolean hasUpdated = false;
        for (String field : updatedRecord.getAvailableFields()) {
            if (whenUpdateFields.contains(field)) {
                hasUpdated = true;
                break;
            }
        }
        return hasUpdated;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "@" + Integer.toHexString(hashCode()) + "#" + actionContext.getConfigId();
    }
}

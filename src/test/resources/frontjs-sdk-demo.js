



const demoEntityName = 'Account' 
const demoFieldName = 'AccountName' 

let _List, _Form, _View

FrontJS.ready(() => {
  _List = FrontJS.DataList
  _Form = FrontJS.Form
  _View = FrontJS.View

  demoAddButton()

  demoForList()

  demoForForm()

  
  _Form.onOpen(() => {
    _Form.setTopAlert('表单顶部显示提示')
  })

  
  setTimeout(() => {
    console.log(FrontJS.getPageToken())
    console.log(FrontJS.getRecord(rb.currentUser))
    console.log(FrontJS.checkPermission(rb.currentUser, 'D'))
  })
})



function demoAddButton() {
  _List.onOpen(() => {
    _List.addButtonGroup({
      text: 'FrontJS!',
      items: [
        {
          text: '获取第一选中',
          onClick: () => {
            alert(_List.getSelectedId())
          },
        },
        {
          text: '获取全部选中',
          onClick: () => {
            alert(_List.getSelectedIds())
          },
        },
      ],
    })
  })

  _Form.onOpen(() => {
    _Form.addButton({
      text: 'FrontJS!',
      onClick: () => {
        alert(_Form.getCurrentId())
      },
    })
  })

  _View.onOpen(() => {
    _Form.addButton({
      text: 'FrontJS!',
      onClick: () => {
        alert(_Form.getCurrentId())
      },
    })
  })
}


function demoForList() {
  
  const fieldKey = `${demoEntityName}.${demoFieldName}`
  _List.regCellRender(fieldKey, function (v) {
    
    return <strong className="text-danger">{JSON.stringify(v)}</strong>
  })

  
  _List.onOpen(() => {
    _List.addButton({
      text: 'LiteForm',
      onClick: () => {
        const id = _List.getSelectedId()
        if (!id) {
          alert('请选择一条记录')
          return
        }

        FrontJS.openLiteForm(id, [{ field: demoFieldName, tip: '提示', readonly: true, nullable: true }])
      },
    })
  })
}


function demoForForm() {
  
  const lookFieldKey = `${demoEntityName}.${demoFieldName}`
  _Form.onFieldValueChange(function (fieldKey, fieldValue, recordId) {
    if (lookFieldKey === fieldKey) {
      RbHighbar.create(`记录: ${recordId || ''} 的新值 : ${fieldValue || ''}`)
    }
  })

  _Form.onOpen(() => {
    
    const fieldComp = _Form.getFieldComp(demoFieldName)

    
    fieldComp.setNullable(false)
    
    fieldComp.setTip('危险品')
    fieldComp.setTip(<b className="text-danger">危险品</b>)

    
    _Form.onFieldValueChange(function (fieldKey, fieldValue) {
      if (lookFieldKey === fieldKey) {
        
        if (fieldValue === 'hide') {
          fieldComp.setHidden(true)

          
          setTimeout(() => fieldComp.setHidden(false), 3000)
        }
      }
    })
  })
}
